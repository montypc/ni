from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import QVariant
import sys
import traceback
import logging

import plugins
from misc import *

class Plugin:
    name       = None
    dockWidget = None
    settings   = None
    winMain    = None
    loaded     = None
    listeners  = None
    monty      = None
    DEFAULTS   = {}

    def __init__(self, winMain, name):
        self.name      = name
        self.winMain   = winMain
        self.loaded    = False
        self.listeners = []
        self.monty     = winMain.monty
        self.settings  = QtCore.QSettings(ORGNAME, APPNAME)

        #init settings
        self.settings.beginGroup(self.name)
        for key in self.DEFAULTS:
            if not self.settings.contains(key):
                self.settings.setValue(key, QVariant(self.DEFAULTS[key]))
        self.settings.endGroup()

    def getName(self):
        return self.name
    def getInfo(self):
        return ''
    def getExtInfo(self):
        return ''
    def getWinMain(self):
        return self.winMain
    def setStatus(self, status):
        self.winMain.setStatus(status)

    def load(self):
        logging.info("loading")
        if len(self.listeners):
            logging.debug("adding %s listeners"%(len(self.listeners)))
            for listener in self.listeners:
                self.monty.addListener(listener[0], listener[1])

        self._load()
        opts=QtGui.QDockWidget.DockWidgetClosable|QtGui.QDockWidget.DockWidgetMovable
        self.winMain.addDock(self.getDockWidget(opts))
        self.loaded=True
    def unload(self):
        if not self.loaded:
            return
        logging.info("unloading")
        if len(self.listeners):
            logging.debug("removing %s listeners"%(len(self.listeners)))
            for listener in self.listeners:
                self.monty.removeListener(listener[0], listener[1])

        self._unload()
        dock_widget = self.getDockWidget()
        if dock_widget:
            self.winMain.removeDock(dock_widget)
        self.dockWidget     = None
        self.settingsWidget = None
        self.loaded         = False
    def isLoaded(self):
        return self.loaded

    def addMontyListener(self, event, callback):
        self.listeners.append([event, callback])

    def getDockWidget(self, opts=None):
        self.dockWidget = self._getDockWidget()
        if self.dockWidget and opts:
            self.dockWidget.setFeatures(opts)
            self.dockWidget.setAllowedAreas(QtCore.Qt.AllDockWidgetAreas)
        return self.dockWidget

    class SettingsWidget(QtGui.QWidget):
        """ plugins should subclass this"""
        plugin = None
        settings = None

        def __init__(self, plugin):
            QtGui.QWidget.__init__(self)
            self.plugin = plugin
            self.settings = QtCore.QSettings(ORGNAME, APPNAME)

        def save_settings(self):
            """ reimplement this"""
            self.plugin.saveSettings()

        def _add_widget(self, widget, label = '', tooltip = ''):
            """adds a widget with label"""
            if not self.layout():
                logging.error('Attempted to call add_widget with no layout set.')
            widget.setToolTip(tooltip)
            layout = QtGui.QHBoxLayout()
            layout.addWidget(QtGui.QLabel(label))
            layout.addWidget(widget)
            self.layout().addLayout(layout)

    def get_settings_widget(self):
        """Should return subclassed SettingsWidget."""
        return

    def resetSettingCache(self):
        #self.settings=None
        self.settingsWidget=None

    def _getPluginClassname(self, cl):
        """Returns the name of a plugin (without 'plugin'-prefix)"""
        return str(cl).split('.')[-1].lower()[len('plugin'):]

    def _getDockWidget(self):
        """Override this one."""
        return None
    def _createDock(self, widget):
        """Creates a QDockWidget with parent $parent containing widget $widget."""
        dock=QtGui.QDockWidget(self.name, self.winMain)
        dock.setObjectName(self.name)
        dock.setWidget(widget)

        return dock
    def _load(self):
        """Override this one."""
        return
    def _unload(self):
        """Override this one."""
        return

