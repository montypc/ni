#!/usr/bin/python

# montypc.py

import sys
from PyQt4 import QtGui
from winMain import winMain
from traceback import print_exc
from misc import APPNAME


def main():
    try:
        app = QtGui.QApplication(sys.argv)
        app.setApplicationName(APPNAME)

        wMain = winMain()
        wMain.show()
        app.exec_()
        wMain.quit()
    except Exception, e:
        print_exc()

if __name__ == "__main__":
    main()
