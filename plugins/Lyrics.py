from PyQt4 import QtGui,QtCore
from PyQt4.QtCore import QVariant

import re
import os
import os.path
from thread import start_new_thread
from traceback import print_exc
import webbrowser
import urllib
import logging

from misc import *
from clPlugin import Plugin

class ResetEvent(QtCore.QEvent):
    song=None
    def __init__(self, song=None):
        QtCore.QEvent.__init__(self,QtCore.QEvent.User)
        self.song=song
class AddHtmlEvent(QtCore.QEvent):
    html=None
    def __init__(self,html):
        QtCore.QEvent.__init__(self,QtCore.QEvent.User)
        self.html=html
class AddTextEvent(QtCore.QEvent):
    text=None
    def __init__(self,text):
        QtCore.QEvent.__init__(self,QtCore.QEvent.User)
        self.text=text

class wgLyrics(QtGui.QWidget):
    " contains the lyrics"
    txtView=None    # text-object
    curLyrics=None  # current lyrics
    btnEdit=None
    btnRefetch=None
    btnSave=None
    btnSearch=None
    editMode=False
    lyFormat=None
    p=None  # plugin
    def __init__(self, p, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.p=p
        self.curLyrics=""
        self.btnEdit=Button("Edit lyrics", self.onBtnEditClick)
        self.btnRefetch=Button("Refetch", self.onBtnRefetchClick)
        self.btnSave=Button("Save lyrics", self.onBtnSaveClick)
        self.btnSearch=Button("Search www", self.onBtnSearch)

        self.txtView=QtGui.QTextEdit(parent)
        self.txtView.setReadOnly(True)

        self.setMode(False)

        layout=QtGui.QVBoxLayout()
        hlayout=QtGui.QHBoxLayout()
        layout.addLayout(hlayout)
        hlayout.addWidget(self.btnEdit)
        hlayout.addWidget(self.btnSave)
        hlayout.addWidget(self.btnRefetch)
        hlayout.addWidget(self.btnSearch)
        layout.addWidget(self.txtView)
        self.setLayout(layout)


    def setMode(self, mode):
        self.editMode=mode
        self.btnSave.setVisible(mode)
        self.btnEdit.setVisible(not mode)
        self.txtView.setReadOnly(not mode)

    def onBtnSearch(self):
        SE = self.p.settings.value(self.p.name + '/engine').toString()
        if not SE:
            return
        f=format.compile(SE)
        SE_url=toAscii(f(format.params(self.p.monty.getCurrentSong())))
        webbrowser.open(urllib.quote(SE_url, ":/+?="))

    def onBtnEditClick(self):
        self.setMode(True)
        self.txtView.setPlainText(self.curLyrics)

    def onBtnSaveClick(self):
        self.setMode(False)
        self.curLyrics=self.txtView.toPlainText()
        file=open(self.getLyricsFilePath(self.p.monty.getCurrentSong()), 'w')
        file.write(self.curLyrics)
        file.close()

        # just to test if everything's still fine
        self.fetchLyrics(self.p.monty.getCurrentSong())

    def onBtnRefetchClick(self):
        # force refetch
        lyFName=self.getLyricsFilePath(self.p.monty.getCurrentSong())
        try:
            os.remove(lyFName)
        except:
            pass
        self.refresh()

    def refresh(self):
        # if we're editing while song changes, too bad: we don't save, for the moment!
        if self.editMode:
            self.setMode(False)

        self.lyFormat=format.compile(self.p.settings.value(self.p.name + '/downloadto').toString())
        song = self.p.monty.getCurrentSong()
        try:
            song._data['file']
        except:
            self.resetTxt()
            return

        self.resetTxt(song)
        start_new_thread(self.fetchLyrics, (song,))

    def customEvent(self, event):
        if isinstance(event,ResetEvent):
            self.resetTxt(event.song)
        elif isinstance(event,AddTextEvent):
            self.txtView.insertPlainText(event.text)
        elif isinstance(event,AddHtmlEvent):
            self.txtView.insertHtml(event.html)

    def getLyricsFilePath(self, song):
        params={'music_dir': self.p.settings.value('music_dir').toString()}
        path=self.lyFormat(format.params(song, params))
        return toAscii(os.path.expanduser(path))

    _mutex=QtCore.QMutex()
    _fetchCnt=0
    def fetchLyrics(self, song):
        # only allow 1 instance to look lyrics!
        self._mutex.lock()
        if self._fetchCnt:
            self._mutex.unlock()
            return
        self._fetchCnt=1
        self._mutex.unlock()

        QtCore.QCoreApplication.postEvent(self, ResetEvent(song))

        lyFName=self.getLyricsFilePath(song)
        logging.debug("checking %s"%(lyFName))
        self.curLyrics=""
        # does the file exist? if yes, read that one!
        try:
            file=open(lyFName, 'r')
            self.curLyrics=file.read()
            file.close()
            QtCore.QCoreApplication.postEvent(self, AddTextEvent(self.curLyrics))
            self._fetchCnt=0
            logging.info("Fetch lyrics from file")
            return
        except Exception, e:
            logging.debug("fail - %s"%(str(e)))

        # fetch from inet
        QtCore.QCoreApplication.postEvent(self, AddHtmlEvent('<i>Searching lyrics ...</i>'))
        logging.info("Fetch lyrics from internet")

        lines = unicode(self.p.settings.value(self.p.getName() + '/sites').toString()).split('\n')
        sites = {}
        for line in lines:
            if line.strip():
                sites[line[0:line.find('\t')]]=line[line.find('\t'):].strip()
        # construct URL to search!
        SE = self.p.settings.value(self.p.name + '/engine').toString()
        try:
            ret=fetch(SE, sites, song, {})
            QtCore.QCoreApplication.postEvent(self, ResetEvent(song))
            if ret:
                logging.info("Success!")
                self.curLyrics=ret[0]
                # save for later use!
                if lyFName:
                    # we can't save if the path isn't correct
                    try:
                        logging.info("Saving to %s"%(lyFName))
                        try:
                            # fails when dir exists
                            os.makedirs(os.path.dirname(os.path.expanduser(lyFName)))
                        except Exception, e:
                            pass
                        file=open(lyFName, 'w')
                        file.write(self.curLyrics)
                        file.close()
                    except Exception, e:
                        # probably a wrong path!
                        logging.info("Failed to write lyrics %s"%(str(e)))
            else:
                self.curLyrics=""
                txt="No lyrics found :'("
                logging.info("Lyrics not found!")
                QtCore.QCoreApplication.postEvent(self, AddTextEvent(txt))

            QtCore.QCoreApplication.postEvent(self, AddTextEvent(self.curLyrics))
            if ret:
                QtCore.QCoreApplication.postEvent(self, AddHtmlEvent('<br /><br /><a href="%s">%s</a>'%(ret[1],ret[1])))

        except Exception, e:
            QtCore.QCoreApplication.postEvent(self, ResetEvent(song))
            QtCore.QCoreApplication.postEvent(self, AddHtmlEvent('Woops, error! Possible causes:'\
                    '<br />no internet connection?'\
                    '<br />site unavailable'\
                    '<br />an error in the fetching regular expression'\
                    '<br />(exception: %s)'%(str(e))))
        self._fetchCnt=0

    def resetTxt(self, song=None):
        self.txtView.clear()
        if song:
            self.txtView.insertHtml('<b>%s</b>\n<br /><u>%s</u><br />'\
                    '<br />\n\n'%(song.getTitle(), song.getArtist()))

    def autoScroll(self, time):
        t=self.txtView
        max=t.verticalScrollBar().maximum()
        if max<=0:
            return
        t.verticalScrollBar().setValue((max+t.height()/t.currentFont().pointSize())*time/self.p.monty.getCurrentSong()._data['time'])



class pluginLyrics(Plugin):
    o = None
    DEFAULTS = {'engine' : 'http://www.google.com/search?q=lyrics+"$artist"+"$title"',
                'sites'  : 'absolutelyrics.com <div id="realText">(.*?)</div>\n'\
                           'azlyrics.com   <br><br>.*?<br><br>(.*?)<br><br>\n'\
                           'oldielyrics.com    song_in_top2.*?<br>(.*?)<script\n'\
                           'lyricstime.com phone-left.gif.*?<p>(.*?)</p>\n'\
                           'lyricsfire.com class="lyric">.*?Song.*?<br>(.*?)</pre>\n'\
                           'lyricsfreak.com    <div.*?id="content".*?>(.*?)<blockquote\n'\
                           'artists.letssingit.com <pre>(.*?)</pre>\n'\
                           'gugalyrics.com </h1>(.*?)<a\n'\
                           'lyricsmania.com    </strong> :(.*?)&#91;\n'\
                           'leoslyrics.com <font face=.*?size=-1>(.*?)</font>\n'\
                           'bluesforpeace.com  <blockquote>(.*?)</blockquote>\n',
                'target' : '~/.lyrics/$artist/$artist - $title.txt'}


    def __init__(self, winMain):
        Plugin.__init__(self, winMain, 'Lyrics')
        self.addMontyListener('onSongChange', self.refresh)
        self.addMontyListener('onReady', self.refresh)
        self.addMontyListener('onDisconnect', self.onDisconnect)
        self.addMontyListener('onTimeChange', self.onTimeChange)
    def _load(self):
        self.o=wgLyrics(self, None)
        self.o.refresh()
    def _unload(self):
        self.o=None
    def getInfo(self):
        return "Show (and fetch) the lyrics of the currently playing song."

    def _getDockWidget(self):
        return self._createDock(self.o)

    def refresh(self, params):
        self.o.refresh()
    def onDisconnect(self, params):
        self.o.resetTxt()
    def onTimeChange(self, params):
        if self.settings.value(self.name + '/autoscroll').toBool() and (not self.o.editMode):
            # could be done better, but this is just plain simple :)
            self.o.autoScroll(params['newTime'])

    class SettingsWidgetLyrics(Plugin.SettingsWidget):
        sites = None
        autoscroll = None
        engine = None
        target = None

        def __init__(self, plugin):
            Plugin.SettingsWidget.__init__(self, plugin)
            self.settings.beginGroup(self.plugin.getName())

            self.sites = QtGui.QTextEdit()
            self.sites.insertPlainText(self.settings.value('sites').toString())

            self.autoscroll = QtGui.QCheckBox('Autoscroll')
            self.autoscroll.setChecked(self.settings.value('autoscroll').toBool())

            self.engine = QtGui.QLineEdit(self.settings.value('engine').toString())
            self.target = QtGui.QLineEdit(self.settings.value('target').toString())

            self.settings.endGroup()

            self.setLayout(QtGui.QVBoxLayout())
            self.layout().addWidget(self.engine)
            self.layout().addWidget(self.sites)
            self.layout().addWidget(self.target)
            self.layout().addWidget(self.autoscroll)

        def save_settings(self):
            self.settings.beginGroup(self.plugin.getName())
            self.settings.setValue('engine', QVariant(self.engine.text()))
            self.settings.setValue('sites',  QVariant(self.sites.toPlainText()))
            self.settings.setValue('target', QVariant(self.target.text()))
            self.settings.setValue('autoscroll', QVariant(self.autoscroll.isChecked()))
            self.settings.endGroup()
            self.plugin.o.refresh()

    def get_settings_widget(self):
        return self.SettingsWidgetLyrics(self)



