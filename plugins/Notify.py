from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import QVariant
from traceback import print_exc

from misc import *
from clPlugin import Plugin
import plugins

NOTIFY_PRIORITY_SONG = 1
NOTIFY_PRIORITY_VOLUME = 2

class winNotify(QtGui.QWidget):
    _timerID=None
    winMain=None
    p=None

    _current_priority = 0

    timer=None

    cover_label = None
    text_label  = None

    def __init__(self, p, winMain, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.p=p
        self.winMain=winMain

        layout = QtGui.QHBoxLayout()
        self.cover_label = QtGui.QLabel()
        self.text_label  = QtGui.QLabel()
        self.text_label.setWordWrap(True)
        layout.addWidget(self.cover_label)
        layout.addWidget(self.text_label)
        self.setLayout(layout)

        self.setWindowFlags(QtCore.Qt.ToolTip | QtCore.Qt.WindowStaysOnTopHint)
        self.setWindowOpacity(0.7)

        font = QtGui.QFont()
        font.setPixelSize(20)
        self.setFont(font)

    def mousePressEvent(self, event):
        self.hide()

    def show(self, text, time = 3, priority = 0):
        if not priority >= self._current_priority:
            return
        self._current_priority = priority

        cover = plugins.getPlugin('albumcover').getWidget().get_cover()
        if cover:
            self.cover_label.setPixmap(cover.scaledToHeight(self.fontInfo().pixelSize()*4))
        else:
            self.cover_label.clear()

        self.text_label.setText(text)
        if self._timerID:
            self.killTimer(self._timerID)
        self._timerID=self.startTimer(500)
        self.timer = time*2
        self.resize(self.layout().sizeHint())
        self.centerH()
        self.setVisible(True)
        self.timerEvent(None)

    def hide(self):
        if self._timerID:
            self.killTimer(self._timerID)
            self._timerID=None
        self.setHidden(True)
        self._current_priority = -1

    def centerH(self):
        screen = QtGui.QDesktopWidget().screenGeometry()
        size = self.geometry()
        self.move((screen.width()-size.width())/2, 100)

    def timerEvent(self, event):
        self.timer-=1
        if self.timer<=0:
            self.hide()
        self.update()

class pluginNotify(Plugin):
    o=None
    DEFAULTS = {'songformat' : '$track - $artist - $title ($album) [$length]',
                'timer'      : 3}
    def __init__(self, winMain):
        Plugin.__init__(self, winMain, 'Notify')
        self.addMontyListener('onSongChange', self.onSongChange)
        self.addMontyListener('onReady', self.onReady)
        self.addMontyListener('onDisconnect', self.onDisconnect)
        self.addMontyListener('onStateChange', self.onStateChange)
        self.addMontyListener('onVolumeChange', self.onVolumeChange)

    def _load(self):
        self.o = winNotify(self, self.winMain)
    def _unload(self):
        self.o=None
    def getInfo(self):
        return "Show interesting events in a popup window."

    def onSongChange(self, params):
        song = self.monty.getCurrentSong()
        if not song:
            return
        self.settings.beginGroup(self.name)
        self.o.show(song.expand_tags(self.settings.value('songformat').toString()), self.settings.value('timer').toInt()[0],
                    NOTIFY_PRIORITY_SONG)
        self.settings.endGroup()

    def onReady(self, params):
        self.o.show('montypc loaded!', self.settings.value(self.name + '/timer').toInt()[0])

    def onDisconnect(self, params):
        self.o.show('Disconnected!', self.settings.value(self.name + '/timer').toInt()[0])

    def onStateChange(self, params):
        self.o.show(params['newState'], self.settings.value(self.name + '/timer').toInt()[0])

    def onVolumeChange(self, params):
        self.o.show('Volume: %i%%'%(params['newVolume']), self.settings.value(self.name + '/timer').toInt()[0], priority = NOTIFY_PRIORITY_VOLUME)

    class SettingsWidgetNotify(Plugin.SettingsWidget):
        format = None
        timer  = None

        def __init__(self, plugin):
            Plugin.SettingsWidget.__init__(self, plugin)
            self.settings.beginGroup(self.plugin.getName())

            self.format = QtGui.QLineEdit(self.settings.value('songformat').toString())

            self.timer = QtGui.QLineEdit(self.settings.value('timer').toString())
            self.timer.setValidator(QtGui.QIntValidator(self.timer))

            self.setLayout(QtGui.QVBoxLayout())
            self.layout().addWidget(self.format)
            self.layout().addWidget(self.timer)
            self.settings.endGroup()

        def save_settings(self):
            self.settings.beginGroup(self.plugin.getName())
            self.settings.setValue('songformat', QVariant(self.format.text()))
            self.settings.setValue('timer', QVariant(self.timer.text().toInt()[0]))
            self.settings.endGroup()
            self.plugin.onSongChange(None)

    def get_settings_widget(self):
        return self.SettingsWidgetNotify(self)
