from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import QVariant
import logging

from clPlugin import Plugin
from misc import *
from wgPlaylist import Playlist
import plugins

# Dependencies:
#   playcontrol
class pluginPlaylist(Plugin, plugins.IPlaylist):
    o            = None
    clr_plst_btn = None
    DEFAULTS = {'modes' : '$artist\n'\
                          '$artist/$date - $album\n'\
                          '$artist - $album\n'\
                          '$album ($artist)\n'\
                          '$genre\n'\
                          '$genre/$artist\n'\
                          '$genre/$artist - $album\n'}

    def __init__(self, winMain):
        Plugin.__init__(self, winMain, 'Playlist')
        self.addMontyListener('onSongChange', self.onSongChange)
        self.addMontyListener('onPlaylistChange', self.on_playlist_change)
        self.addMontyListener('onDisconnect', self.on_playlist_change)
    def _load(self):
        self.o=Playlist(self.winMain, self, ['artist', 'title', 'track', 'album'], 'Playlist'
                , self.onDoubleClick, self.onKeyPress, unicode(self.settings.value('modes').toString().split('\n')))

        # clear playlist button
        self.clrPlstBtn = QtGui.QPushButton(QtGui.QIcon('gfx/button_cancel.png'), '')
        self.clrPlstBtn.setToolTip("Clear playlist")
        self.o.connect(self.clrPlstBtn, QtCore.SIGNAL('clicked()'), self.clearPlaylistPressed)
        self.clrPlstBtn.palette().setColor(QtGui.QPalette.Button, self.o.palette().color(QtGui.QPalette.Window))
        self.o.filterLayout.insertWidget(0, self.clrPlstBtn)

    def _unload(self):
        self.o=None
    def getPlaylist(self):
        return self.o
    def getInfo(self):
        return "The playlist showing the songs that will be played."

    def getList(self):
        return self.o

    def _getDockWidget(self):
        return self._createDock(self.o)

    def onDoubleClick(self):
        self.monty.play(self.o.getSelItemID())

    def onKeyPress(self, event):
        if event.matches(QtGui.QKeySequence.Delete):
            # remove selection from playlist using DELETE-key
            ids=self.o.selectedIds()
            self.setStatus('Deleting '+str(len(ids))+' songs from playlist ...')
            doEvents()

            self.monty.deleteFromPlaylist(ids)

            self.setStatus('')
            doEvents()

        elif event.key()==QtCore.Qt.Key_Q:
            # queue selected songs
            # Hoho, this one needs the playcontrol plugin!
            plugins.getPlugin('playcontrol').addSongsToQueue(self.o.selectedIds())
        return QtGui.QWidget.keyPressEvent(self.o, event)

    def clearPlaylistPressed(self):
        self.setStatus('Clearing playlist...')
        doEvents()
        self.monty.clear_playlist()
        self.setStatus('')
        doEvents()

    def onSongChange(self, params):
        lst=self.o
        lst.colorID(int(params['newSongID']), self.o.palette().color(QtGui.QPalette.Highlight))

        if params['newSongID']!=-1:
            lst.ensureVisible(params['newSongID'])

    _rowColorModifier=0
    _rowColorAdder=1
    def timerEvent(self, event):
        curSong=self.monty.getCurrentSong()
        if curSong:
            lst=self.lstPlaylist
            # color current playing song
            lst.colorID(curSong.getID(), self.o.palette().color(QtGui.QPalette.Highlight))

            # make sure color changes nicely over time
            self._rowColorModifier=self._rowColorModifier+self._rowColorAdder
            if abs(self._rowColorModifier)>4:
                self._rowColorAdder=-1*self._rowColorAdder

    def ensureVisible(self, id):
        """Make sure song with ID id is visible."""
        self.o.ensureVisible(id)

    def on_playlist_change(self, params = None):
       self.getList().updateSongs(self.monty.listPlaylist())

    class SettingsWidgetPlaylist(Plugin.SettingsWidget):
        modes = None
        def __init__(self, plugin):
            Plugin.SettingsWidget.__init__(self, plugin)
            self.setLayout(QtGui.QVBoxLayout())

            self.modes = QtGui.QTextEdit()
            self.modes.insertPlainText(self.settings.value(self.plugin.getName() + '/modes').toString())
            self.layout().addWidget(self.modes)

        def save_settings(self):
            self.settings.setValue(self.plugin.getName() + '/modes', QVariant(self.modes.toPlainText()))
            self.plugin.o.setModes(unicode(self.settings.value(self.plugin.getName() + '/modes').toString().split('\n')))

    def get_settings_widget(self):
        return self.SettingsWidgetPlaylist(self)
