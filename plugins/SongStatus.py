from PyQt4 import QtGui
from clPlugin import *
from traceback import print_exc

import format

SONGSTATUS_FORMAT_DEFAULT='<font size="4">now $state</font>'\
'$if($title,<font size="8" color="blue">$title</font>'\
'<br />by <font size="8" color="green">$artist</font>'\
'<br /><font size="5" color="red">[$album # $track]</font>)'\
'$if($length,<br /><font size="4">$time/$length</font>)'

class wgSongStatus(QtGui.QWidget):
    """Displays the status of the current song, if playing."""
    " label containing the info"
    lblInfo=None
    format=None
    p=None
    def __init__(self, p, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.p=p

        self.lblInfo=QtGui.QLabel()
        self.setMinimumWidth(400)

        layout=QtGui.QHBoxLayout()
        self.setLayout(layout)

        layout.addWidget(self.lblInfo)
        self.updateFormat()

    def update(self, params):
        status = self.p.monty.getStatus()
        song = self.p.monty.getCurrentSong()

        values={'state':''}
        try:
            values['state']={'play':'playing', 'stop':'stopped', 'pause':'paused'}[status['state']]
            if 'time' in status:
                values['length']=sec2min(status['length'])
                values['time']=sec2min(status['time'])
        except:
            pass
        
        if song:
            self.lblInfo.setText(self.format(format.params(song, values)))

    def updateFormat(self):
        try:
            self.format=format.compile(self.p.getSetting('format'))
        except Exception, e:
            self.format=lambda p: "Invalid format: %s"%(e)

    def text(self):
        return self.lblInfo.text()

class pluginSongStatus(Plugin):
    o=None
    def __init__(self, winMain):
        Plugin.__init__(self, winMain, 'SongStatus')
        self.addMontyListener('onSongChange', self.update)
        self.addMontyListener('onTimeChange', self.update)
        self.addMontyListener('onStateChange', self.update)
        self.addMontyListener('onConnect', self.update)
        self.addMontyListener('onDisconnect', self.update)
    
    def _load(self):
        self.o=wgSongStatus(self, None)
        self.update(None)
    def _unload(self):
        self.o=None
    def getInfo(self):
        return "Show information about the current song."
    
    def update(self, params):
        self.o.update(params)
    
    def _getDockWidget(self):
        return self._createDock(self.o)

    def _getSettings(self):
        format=QtGui.QTextEdit()
        format.insertPlainText(self.getSetting('format'))
        return [
                ['format', 'Format', 'Format of the song status. Possible tags: $title, $artist, $album, $track, $time, $length, $state', format]
            ]
    def afterSaveSettings(self):
        self.o.updateFormat()
        self.o.update(None)
