from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import QVariant

from clPlugin import Plugin
from misc import *
from wgPlaylist import Playlist

class pluginLibrary(Plugin):
    o=None
    DEFAULTS = {'modes' : '$artist\n'\
                          '$artist/$date - $album\n'\
                          '$artist - $album\n'\
                          '$album ($artist)\n'\
                          '$genre\n'\
                          '$genre/$artist\n'\
                          '$genre/$artist - $album\n'}

    def __init__(self, winMain):
        Plugin.__init__(self, winMain, 'Library')
        self.addMontyListener('onReady', self.refresh)
        self.addMontyListener('onDisconnect', self.refresh)
        self.addMontyListener('onUpdateDBFinish', self.refresh)
        self.settings = QtCore.QSettings(ORGNAME, APPNAME)
    def _load(self):
        self.o=Playlist(self.winMain, self, ['song'], 'Library'
                , self.onDoubleClick, self.onKeyPress, unicode(self.settings.value(self.name + '/modes').toString()).split('\n'))
    def _unload(self):
        self.o=None
    def getInfo(self):
        return "List showing all the songs allowing filtering and grouping."

    def getList(self):
        return self.o

    def _getDockWidget(self):
        return self._createDock(self.o)

    def onDoubleClick(self):
        self.addLibrarySelToPlaylist()

    def onKeyPress(self, event):
        # Add selection, or entire library to playlist using ENTER-key.
        if event.key()==QtCore.Qt.Key_Enter or event.key()==QtCore.Qt.Key_Return:
            self.addLibrarySelToPlaylist()
        return QtGui.QWidget.keyPressEvent(self.o, event)

    def addLibrarySelToPlaylist(self):
        """Add the library selection to the playlist."""
        songs=self.o.selectedSongs()
        self.setStatus('Adding '+str(len(songs))+' songs to library ...')
        doEvents()

        # add filepaths of selected songs to path
        paths=map(lambda song: unicode(song.getFilepath()), songs)
        # add in chunks of 1000
        CHUNK_SIZE=1000
        start=0
        while start<len(paths):
            if start+CHUNK_SIZE<len(paths):
                end=start+CHUNK_SIZE
            else:
                end=len(paths)
            self.setStatus('Adding '+str(len(songs))+' songs to library: %i%%'%(int(100*start/len(paths))))
            doEvents()
            ids = self.monty.addToPlaylist(paths[start:end])
            start+=CHUNK_SIZE

        self.setStatus('')
        doEvents()
        if (not self.monty.isPlaying()) and ids:
            self.monty.play(ids[0])

    def refresh(self, params = None):
        self.o.updateSongs(self.monty.listLibrary())

    class SettingsWidgetLibrary(Plugin.SettingsWidget):
        modes = None
        def __init__(self, plugin):
            Plugin.SettingsWidget.__init__(self, plugin)
            self.setLayout(QtGui.QVBoxLayout())

            self.modes = QtGui.QTextEdit()
            self.modes.insertPlainText(self.settings.value(self.plugin.getName() + '/modes').toString())
            self.layout().addWidget(self.modes)

        def save_settings(self):
            self.settings.setValue(self.plugin.getName() + '/modes', QVariant(self.modes.toPlainText()))
            self.plugin.o.setModes(unicode(self.settings.value(self.plugin.getName() + '/modes').toString()).split('\n'))

    def get_settings_widget(self):
        return self.SettingsWidgetLibrary(self)
