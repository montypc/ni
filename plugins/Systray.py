from PyQt4 import QtGui
from PyQt4.QtCore import QVariant

from clPlugin import Plugin
from misc import *

class pluginSystray(Plugin):
    DEFAULTS = {'format': '$track - $title by $artist on $album ($length)'}
    o        = None
    format   = None
    eventObj = None
    time     = None   # indicator of current time [0..64]
    appIcon  = None
    pixmap   = None
    def __init__(self, winMain):
        Plugin.__init__(self, winMain, 'Systray')
        self.addMontyListener('onSongChange', self.update)
        self.addMontyListener('onReady',      self.update)
        self.addMontyListener('onConnect',    self.update)
        self.addMontyListener('onDisconnect', self.update)
        self.addMontyListener('onTimeChange', self.update) # TODO only update this when necessary, i.e. mouse-hover etc
        self.appIcon=QtGui.QIcon(appIcon)

    def _load(self):
        self.format = self.settings.value(self.name + '/format').toString()
        class SystrayWheelEventObject(QtCore.QObject):
            """This class listens for systray-wheel events"""
            def eventFilter(self, object, event):
                if type(event)==QtGui.QWheelEvent:
                    numDegrees=event.delta() / 8
                    numSteps=5*numDegrees/15
                    self.plugin.monty.setVolume(self.plugin.monty.getVolume() + numSteps)
                    event.accept()
                    return True
                return False

        self.o=QtGui.QSystemTrayIcon(QtGui.QIcon(appIcon), self.winMain)
        self.eventObj=SystrayWheelEventObject()
        self.eventObj.plugin = self
        self.o.installEventFilter(self.eventObj)
        self.winMain.connect(self.o, QtCore.SIGNAL('activated (QSystemTrayIcon::ActivationReason)')
            , self.onSysTrayClick)
        self.o.show()
        self.update(None)

    def _unload(self):
        self.o.hide()
        self.o.setIcon(QtGui.QIcon(None))
        self.o=None
        self.winMain._wheelEvent=None
    def getInfo(self):
        return "Display the montypc icon in the systray."

    def update(self, params):
        status = self.monty.getStatus()
        song   = self.monty.getCurrentSong()

        values={'state':''}
        values['state']={'play':'playing', 'stop':'stopped', 'pause':'paused'}[status['state']]
        if 'time' in status:
            values['length']=sec2min(status['length'])
            values['time']=sec2min(status['time'])

        if song:
            self.o.setToolTip(song.expand_tags(self.format))
        else:
            self.o.setToolTip("montypc not playing")

        try:
            curTime=(64*status['time'])/status['length']
        except:
            curTime=-1
        if self.time!=curTime:
            self.time=curTime
            # redraw the systray icon
            self.pixmap=self.appIcon.pixmap(64,64)
            painter=QtGui.QPainter(self.pixmap)
            painter.fillRect(1, curTime, 63, 64, self.winMain.palette().brush(QtGui.QPalette.Base))
            self.appIcon.paint(painter, 1, 0, 63, 64)
            self.o.setIcon(QtGui.QIcon(self.pixmap))

        elif not song:
            self.time=None
            self.o.setIcon(QtGui.QIcon(appIcon))

    def onSysTrayClick(self, reason):
        if reason==QtGui.QSystemTrayIcon.Trigger \
            or reason==QtGui.QSystemTrayIcon.Context:
            w=self.getWinMain()
            # left mouse button
            if w.isVisible():
                settings.setIntTuple('winMain.pos', w.x(), w.y())
                w.setVisible(False)
            else:
                w.setVisible(True)
                try:
                    x,y=settings.getIntTuple('winMain.pos')
                except:
                    x,y=0,0
                w.move(x, y)
        elif reason==QtGui.QSystemTrayIcon.MiddleClick:
            # middle mouse button
            if self.monty.isPlaying():
                self.monty.pause()
            else:
                self.monty.resume()

    class SettingsWidgetSystray(Plugin.SettingsWidget):
        format = None

        def __init__(self, plugin):
            Plugin.SettingsWidget.__init__(self, plugin)

            self.format = QtGui.QLineEdit(self.settings.value(self.plugin.getName() + '/format').toString())

        def save_settings(self):
            self.settings.beginGroup(self.plugin.getName())
            self.settings.setValue('format', QVariant(self.format.text()))
            self.settings.endGroup()

    def get_settings_widget(self):
        return self.SettingsWidgetSystray(self)
