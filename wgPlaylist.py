from PyQt4 import QtGui, QtCore
from thread import start_new_thread

from wgSongList import SongList

from misc import *
from clSettings import settings


class Playlist(QtGui.QWidget):
    """The Playlist widget is a list optimized for displaying a playlist, with filtering."""
    name=None   # name of this playlist
    txtFilter=None  # filter input
    btnClearFilter=None
    cmbMode=None    # what mode? library, playlist?
    modes=[]
    lstSongs=None   # list
    winMain=None    # the main window
    onKeyPress=None
    _timerID=None
    frameLayout = None
    filterLayout = None

    def __init__(self, parent, wMain, headers, name, onDoubleClick, onKeyPress, modes):
        QtGui.QWidget.__init__(self, parent)

        self.name=name
        self.winMain=wMain

        self.onKeyPress=onKeyPress

        self.lstSongs=SongList(parent, self.name, headers, onDoubleClick)

        self.txtFilter=QtGui.QLineEdit()

        self.btnClearFilter = QtGui.QPushButton(QtGui.QIcon('gfx/clear_left.png'), "")
        self.btnClearFilter.palette().setColor(QtGui.QPalette.Button, self.palette().color(QtGui.QPalette.Window))
        self.btnClearFilter.connect(self.btnClearFilter, QtCore.SIGNAL('clicked(bool)'), self.clearTxtFilter)

        self.cmbMode=QtGui.QComboBox()
        self.setModes(modes)

        self.frameLayout = QtGui.QVBoxLayout()
        self.frameLayout.setSpacing(1)
        self.frameLayout.setMargin(0)
        self.filterLayout = QtGui.QHBoxLayout()
        self.filterLayout.setSpacing(0)
        self.filterLayout.addWidget(self.txtFilter)
        self.filterLayout.addWidget(self.btnClearFilter)
        self.frameLayout.addWidget(self.cmbMode)
        self.frameLayout.addLayout(self.filterLayout);
        self.frameLayout.addWidget(self.lstSongs)
        self.setLayout(self.frameLayout)

        self.connect(self.txtFilter, QtCore.SIGNAL('textChanged(const QString&)'), self.txtFilterChange)
        self.connect(self.txtFilter, QtCore.SIGNAL('editingFinished()'), self.txtFilterQuit)
        self.connect(self.cmbMode, QtCore.SIGNAL('currentIndexChanged(const QString&)'), self.onModeChangeChange)

    def clearTxtFilter(self, event):
        self.txtFilter.setText("")

    _prevTxtFilter=None
    def timerEvent(self, event):
        if self.txtFilter.text()!=self._prevTxtFilter:
            # only filter after a little while
            self.killTimer(self._timerID)
            self._timerID=None
            self.filter()
    def keyPressEvent(self, event):
        self.onKeyPress(event)
    def txtFilterChange(self):
        if self._timerID==None:
            # only start filtering after a little while
            self._timerID=self.startTimer(500)
    def txtFilterQuit(self):
        if self._timerID:
            self.killTimer(self._timerID)
            self._timerID=None
    def updateSongs(self, songs):
        """Update the list of songs."""
        start_new_thread(self._update, (songs,))
    def _update(self, songs):
        self.lstSongs.updateSongs(songs)
        if len(self.txtFilter.text()):
            self.filter()

    def setMode(self, mode, levels=''):
        """Set the mode of showing the list."""
        if mode=='playlist':
            self.cmbMode.setCurrentIndex(0)
        else:
            self.cmbMode.setCurrentIndex(self.cmbMode.findText(levels))
    def selectedSongs(self):
        """Get the selected songs."""
        return self.lstSongs.selectedSongs()
    def selectedIds(self):
        """Get the selected songIDs."""
        return map(lambda song: song._data['id'], self.lstSongs.selectedSongs())
    def getSelItemID(self):
        """Get the id of the first selected song."""
        try:
            return self.lstSongs.selectedSongs()[0].getID()
        except:
            return -1

    def filter(self):
        """Filter the songs according to the inputbox."""
        self.lstSongs.killFilters()
        start_new_thread(self.lstSongs.filter, (str(self.txtFilter.text()).strip().lower(), ))

    def colorID(self, id, clr):
        """Color song with ID $id with color $clr."""
        self.lstSongs.colorID(id, clr)

    def onModeChangeChange(self, value):
        if value=='playlist':
            self.lstSongs.setMode('playlist')
        else:
            self.lstSongs.setMode('library', self.modes[self.cmbMode.currentIndex()])
        settings.set("l%s.mode"%(self.name), self.cmbMode.currentIndex())
        self.filter()

    def showColumn(self, column, show=True):
        """Show or hide column $column."""
        self.lstSongs.showColumn(column, show)
    def selectRow(self, row):
        """Select row $row."""
        self.lstSongs.selectRow(row)
    def ensureVisible(self, id):
        """Make sure song with ID $id is visible."""
        self.lstSongs.ensureVisible(id)

    def setModes(self, modes):
        i=int(settings.get("l%s.mode"%(self.name), 0))
        self.modes=['playlist']
        self.modes.extend(modes)
        self.cmbMode.clear()
        self.cmbMode.addItem('playlist')
        for mode in modes:
            if mode:
                self.cmbMode.addItem(mode.replace('$', ''))
        self.cmbMode.setCurrentIndex(i)
        self.onModeChangeChange(str(self.cmbMode.itemText(i)))
