from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import QVariant
import os

from misc import ORGNAME, APPNAME, Button, appIcon, doEvents
import plugins


class winSettings(QtGui.QWidget):
    btnSave=None
    btnClose=None
    lstPlugins=None

    winMain=None
    settings = None
    settings_wg = []

    class SettingsWidgetMPD(QtGui.QWidget):
        monty    = None
        settings = None
        host_txt = None
        port_txt = None
        lib_txt  = None
        update   = None

        def __init__(self, monty):
            QtGui.QWidget.__init__(self)
            self.settings = QtCore.QSettings(ORGNAME, APPNAME)
            self.monty = monty

            self.settings.beginGroup('MPD')
            self.host_txt = QtGui.QLineEdit(self.settings.value('host', QVariant('localhost')).toString())
            self.port_txt = QtGui.QLineEdit(self.settings.value('port', QVariant('6600')).toString())
            self.lib_txt  = QtGui.QLineEdit(self.settings.value('music_dir', QVariant(os.path.expanduser('~/music/'))).toString())
            self.settings.endGroup()

            self.update = QtGui.QPushButton('Update MPD database')
            self.connect(self.update, QtCore.SIGNAL('clicked()'), self.update_db)

            self.setLayout(QtGui.QVBoxLayout())
            self.layout().addWidget(self.host_txt)
            self.layout().addWidget(self.port_txt)
            self.layout().addWidget(self.lib_txt)
            self.layout().addWidget(self.update)

        def save_settings(self):
            self.settings.beginGroup('MPD')
            self.settings.setValue('host', QVariant(self.host_txt.text()))
            self.settings.setValue('port', QVariant(self.port_txt.text()))
            self.settings.setValue('music_dir', QVariant(self.lib_txt.text()))
            self.settings.endGroup()

        def update_db(self):
            self.monty.updateDB()

    def __init__(self, winMain, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.settings = QtCore.QSettings(ORGNAME, APPNAME)
        self.winMain  = winMain

        self.btnSave  = Button('save all', self.onBtnSaveClick)
        self.btnClose = Button('close', self.onBtnCloseClick)

        tabWidget = QtGui.QTabWidget(parent)
        self.settings_wg.append(self.SettingsWidgetMPD(self.winMain.monty))
        tabWidget.addTab(self.settings_wg[-1], 'MPD settings')

        self.lstPlugins = QtGui.QListWidget(self)
        tabWidget.addTab(self.lstPlugins, 'plugins')
        for k,entry in plugins.listPlugins().iteritems():
            plugin=entry[plugins.PLUGIN_INSTANCE]
            if plugin:
                wg = plugin.get_settings_widget()
                if wg:
                    self.settings_wg.append(wg)
                    tabWidget.addTab(self.settings_wg[-1], plugin.getName())
        self.fillList()

        self.setLayout(QtGui.QVBoxLayout())
        self.layout().addWidget(tabWidget)

        layoutButtons = QtGui.QHBoxLayout()
        layoutButtons.addStretch()
        layoutButtons.addWidget(self.btnSave)
        layoutButtons.addWidget(self.btnClose)
        self.layout().addLayout(layoutButtons)

        self.connect(self.lstPlugins, QtCore.SIGNAL('itemChanged (QListWidgetItem*)'), self.onlstPluginItemChanged)

        self.setWindowIcon(QtGui.QIcon(appIcon))
        self.setWindowTitle('Settings')
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.center()
        self.resize(800,400)
        doEvents()

    def fillList(self):
        self.lstPlugins.clear()
        for k,entry in plugins.listPlugins().iteritems():
            plugin=entry[plugins.PLUGIN_INSTANCE]
            if plugin:
                if entry[plugins.PLUGIN_MSG]:
                    item=QtGui.QListWidgetItem("%s\t%s"%(entry[plugins.PLUGIN_CLASS], entry[plugins.PLUGIN_MSG]))
                    item.setCheckState(QtCore.Qt.Unchecked)
                    item.setTextColor(QtCore.Qt.red)
                else:
                    item=QtGui.QListWidgetItem("%s\t%s"%(entry[plugins.PLUGIN_CLASS], plugin.getInfo()))
                    if plugin.isLoaded():
                        item.setCheckState(QtCore.Qt.Checked)
                    else:
                        item.setCheckState(QtCore.Qt.Unchecked)

                    if self.settings.value(plugin.getName() + '/load') == None:
                        # load new plugins by default
                        item.setTextColor(QtCore.Qt.blue)
                        self.settings.setValue(plugin.getName() + '/load', QtCore.QVariant(True))

            else:
                item=QtGui.QListWidgetItem("%s\t%s"%(entry[plugins.PLUGIN_CLASS], entry[plugins.PLUGIN_MSG]))
                item.setCheckState(QtCore.Qt.Unchecked)
                item.setTextColor(QtCore.Qt.red)
            self.lstPlugins.addItem(item)

    def center(self):
        screen = QtGui.QDesktopWidget().screenGeometry()
        size = self.geometry()
        self.move((screen.width()-size.width())/2, (screen.height()-size.height())/2+100)

    def onBtnSaveClick(self):
        for wg in self.settings_wg:
            wg.save_settings()

    def onBtnCloseClick(self):
        self.close()
    def onlstPluginItemChanged(self, item):
        # check here if we have to load or unload the plugin!
        toload = (item.checkState() == QtCore.Qt.Checked)
        className=str(item.text()[0:str(item.text()).find('\t')])
        if toload:
            # refresh the plugin file
            plugin=plugins.loadPlugin(className, self.winMain)
            if plugin:
                plugin.load()
            self.fillList()
            self.winMain.restoreLayout()
        else:
            plugin=plugins.getPlugin(className)
            if plugin:
                plugin.unload()
        if plugin:
            self.settings.setValue(plugin.getName() + '/load', QtCore.QVariant(toload))
    def closeEvent(self, event):
        map(lambda entry: entry[plugins.PLUGIN_INSTANCE] and entry[plugins.PLUGIN_INSTANCE].resetSettingCache(), plugins.listPlugins().values())
        self.settings_wg = None
        self.winMain.wSettings=None
