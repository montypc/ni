from PyQt4 import QtCore
from misc import ORGNAME, APPNAME, sec2min
import os

# compare two songs with respect to their album
def isSameAlbum(song1, song2):
    return song1.getAlbum()==song2.getAlbum() \
            and song1.getTag('date')==song2.getTag('date')

class Song:
    """The Song class offers an abstraction of a song."""
    _data=None

    def __init__(self, data):
        self._data=data
        if 'id' in self._data:
            self._data['id']=int(self._data['id'])
        if 'track' in self._data:
            # make sure the track is a valid number!
            t=self._data['track']
            for i in xrange(len(t)):
                if ord(t[i])<ord('0') or ord(t[i])>ord('9'):
                    try:
                        self._data['track']=int(t[0:i])
                    except TypeError:
                        self._data['track']=-1
                    break
            self._data['track']=int(self._data['track'])

        # ensure all string-values are utf-8 encoded
        for tag in self._data.keys():
            if isinstance(self._data[tag], str):
                self._data[tag]=unicode(self._data[tag], "utf-8")
        if 'time' in self._data:
            self._data['time']   = int(self._data['time'])
            self._data['timems'] = '%i:%i'%(self._data['time'] / 60, self._data['time'] % 60)
            self._data['length'] = sec2min(self._data['time'])

    def getID(self):
        """Get the ID."""
        return self.getTag('id', -1)

    def getTitle(self):
        """Get the title."""
        return self.getTag('title', self._data['file'])

    def getArtist(self):
        """Get the artist."""
        return self.getTag('artist', self._data['file'])

    def getTrack(self):
        """Get the track."""
        return self.getTag('track')

    def getAlbum(self):
        """Get the album."""
        return self.getTag('album')

    def getFilepath(self):
        """Get the filepath."""
        return self._data['file']

    def match(self, str):
        """Checks if the string str matches this song. Assumes str is lowercase."""
        return str.__str__() in self.__str__().lower()

    def __str__(self):
        return "%s - %s [%s]" % (self.getTag('artist'), self.getTag('title'), self.getTag('album'))

    def getTag(self, tag, default=''):
        """Get a tag. If it doesn't exist, return $default."""
        if tag in self._data:
            return self._data[tag]
        if tag=='song':
            return self.__str__()

        return default

    def expand_tags(self, str):
        """Expands tags in form $tag in str."""
        ret = str
        for tag in self._data:
            ret = ret.replace('$' + tag, unicode(self._data[tag]))
        return ret

