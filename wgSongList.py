from PyQt4 import QtGui, QtCore, QtSvg
from PyQt4.QtGui import QPalette

import sys
from traceback import print_exc

from misc import *
from clSong import Song
from clSettings import settings
import format

# constants used for fSongs
LIB_ROW=0
LIB_VALUE=1
LIB_INDENT=2
LIB_NEXTROW=3
LIB_EXPANDED=4  # values: 0, 1 or 2 (==song)
LIB_PARENT=5

class DoUpdate(QtCore.QEvent):
    def __init__(self):
        QtCore.QEvent.__init__(self, QtCore.QEvent.User)
class DoResize(QtCore.QEvent):
    def __init__(self):
        QtCore.QEvent.__init__(self, QtCore.QEvent.User)

class SongList(QtGui.QWidget):
    """The SongList widget is a list optimized for displaying an array of songs, with filtering option."""
    # CONFIGURATION VARIABLES
    " font size in pxl"
    fontSize=12 #TODO:make this selectable
    " height of line in pxl"
    lineHeight = fontSize + 4
    " margin"
    margin=4
    vmargin=(lineHeight-fontSize)/2-1
    " width of the vscrollbar"
    scrollbarWidth=15
    " minimum column width"
    minColumnWidth=50
    " colors for alternating rows"
    colors = []
    " color of selection"
    clrSel = None
    " background color"
    clrBg = None
    " indentation of hierarchy, in pixels"
    indentation=lineHeight

    " what function to call when the list is double clicked"
    onDoubleClick=None

    mode='playlist' # what mode is the songlist in? values: 'playlist', 'library'
    " the headers: ( (header, width, visible)+ )"
    headers=None
    songs=None  # original songs
    numSongs=None   # number of songs

    # 'edited' songs
    # in playlist mode, this can only filtering
    # in library mode, this indicates all entries: (row, tag-value, indentation, next-row, expanded)*
    fSongs=None     # filtered songs
    numVisEntries=None  # number of entries that are visible (including when scrolling)


    levels=[]       # levels from the groupBy in library-mode
    groupByStr=''   # groupBy used in library-mode

    vScrollbar=None
    hScrollbar=None

    topRow=-1
    numRows=-1  # total number of rows that can be visible in 1 time
    selRows=None    # ranges of selected rows: ( (startROw,endRow)* )
    selIDs=None # ranges of selected IDs: [ [startID,endID] ]
    selMiscs=None   # array of indexes for selected non-songs in library mode

    selMode=False   # currently in select mode?
    resizeCol=None  # resizing a column?
    clrID=None  # do we have to color a row with certain ID? [ID, color]
    scrollMult=1    # how many rows do we jump when scrolling by dragging
    xOffset=0   # offset for drawing. Is changed by hScrollbar
    resizeColumn=None   # indicates this column should be recalculated
    redrawID=None   # redraw this ID/row only

    wgGfxAlbum = None
    wgGfxArtist = None

    def __init__(self, parent, name, headers, onDoubleClick):
        QtGui.QWidget.__init__(self, parent)
        self.onDoubleClick=onDoubleClick

        # we receive an array of strings; we convert that to an array of (header, width)
        self.name=name
        # load the headers, and fetch from the settings the width and visibility
        self.headers=map(lambda h: [h, int(settings.get('l%s.%s.width'%(self.name,h),250))
            , settings.get('l%s.%s.visible'%(self.name,h),'1')=='1'], headers)
        self.headers.insert(0, ['id', 30, settings.get('l%s.%s.visible'%(self.name,'id'),'0')=='1'])
        self.songs=None
        self.numSongs=None
        self.fSongs=None
        self.selMiscs=[]
        self.numVisEntries=None
        self.xOffset=0
        self.resizeColumn=None

        self.colors  = [self.palette().color(QPalette.Base), self.palette().color(QPalette.AlternateBase)]
        self.clrSel  = self.palette().color(QPalette.Highlight)
        self.clrBg   = self.palette().color(QPalette.Window)

        self._filters=[]

        self.vScrollbar=QtGui.QScrollBar(QtCore.Qt.Vertical, self)
        self.vScrollbar.setMinimum(0)
        self.vScrollbar.setMaximum(1)
        self.vScrollbar.setValue(0)

        self.hScrollbar=QtGui.QScrollBar(QtCore.Qt.Horizontal, self)
        self.hScrollbar.setMinimum(0)
        self.hScrollbar.setMaximum(1)
        self.hScrollbar.setValue(0)
        self.hScrollbar.setPageStep(200)

        self.topRow=0
        self.numRows=0
        self.selRows=[]
        self.selMode=False
        self.clrID=[-1,0]

        self.updateSongs([])
        doEvents()

        self.connect(self.vScrollbar, QtCore.SIGNAL('valueChanged(int)'),self.onVScroll)
        self.connect(self.hScrollbar, QtCore.SIGNAL('valueChanged(int)'),self.onHScroll)

        self.setMouseTracking(True)
        self.setFocusPolicy(QtCore.Qt.TabFocus or QtCore.Qt.ClickFocus
                or QtCore.Qt.StrongFocus or QtCore.Qt.WheelFocus)

        self.setAttribute(QtCore.Qt.WA_OpaquePaintEvent)
        font=QtGui.QFont()
        font.setPixelSize(self.fontSize)
        font.setFamily('Liberation Sans') #TODO make this selectable
        self.setFont(font)
        self.wgGfxAlbum=QtSvg.QSvgRenderer('gfx/gnome-cd.svg')
        self.wgGfxArtist=QtSvg.QSvgRenderer('gfx/user_icon.svg')

    def sizeHint(self):
        return QtCore.QSize(10000,10000)

    def getSongs(self):
        return self.songs

    def customEvent(self, event):
        if isinstance(event, DoResize):
            self.resizeEvent(None)
            self.update()
        elif isinstance(event, DoUpdate):
            self.update()

    def setMode(self, mode, groupBy=''):
        self.selRows=[]
        self.selIDs=[]
        self.selMode=False

        if mode=='playlist':
            self.fSongs=self.songs
            self.numVisEntries=len(self.fSongs)
        elif mode=='library':
            self.groupBy(groupBy)
        else:
            raise Exception('Unknown mode %' %(mode))

        self.mode=mode
        QtCore.QCoreApplication.postEvent(self, DoResize())

    def groupBy(self, groupBy, strFilter=''):
        self.groupByStr=groupBy
        self.levels=groupBy.split('/')
        strFilter=strFilter.strip()

        l=[]
        formats=[]
        for level in self.levels:
            formats.append(format.compile(level))
        # TODO also take l[1] etc into account?
        U="(Unknown)"
        xtra={"album":U, "artist":U, "date":"", "genre":U}
        compare=lambda left, right: cmp(\
                formats[0](format.params(left)).lower(), \
                formats[0](format.params(right)).lower() \
            )

        songs=self.songs
        if strFilter!='':
            songs=filter(lambda song: song.match(strFilter), songs)
        songs=sorted(songs, compare)

        numLevels=len(self.levels)
        self.fSongs=[[0, 'dummy', 0, -1, False]]
        row=0
        # four levels ought to be enough for everyone
        curLevels=[[None,0], [None,0], [None,0], [None,0]]  # contains the values of current levels
        curLevel=0  # current level we're in
        parents=[-1,-1,-1,-1]   # index of parent
        for song in songs:
            if not(self.songs):
                return
            for level in xrange(numLevels):
                # does the file have the required tag?
                if not formats[level](format.params(song, {}, xtra))==curLevels[level][LIB_ROW]:
                    finalRow=row
                    for i in xrange(level,numLevels):
                        tagValue2=formats[i](format.params(song, {}, xtra))

                        self.fSongs[curLevels[i][1]][LIB_NEXTROW]=finalRow
                        self.fSongs.append([row, tagValue2, i, row+1, 0, parents[i]])
                        parents[i+1]=row

                        row+=1
                        curLevels[i]=[tagValue2, row]
                    curLevel=numLevels
            self.fSongs.append([row, song, curLevel, row+1, 2, parents[curLevel]])
            row+=1

        # update last entries' next-row of each level
        #  If we have e.g. artist/album, then the last artist and last album of that
        #  artist have to be repointed to the end of the list, else problems arise
        #  showing those entries ...
        # indicate for each level whether we have processed that level yet
        processed=[False, False, False, False, False]
        numFSongs=len(self.fSongs)
        for i in xrange(numFSongs-1,0,-1):
            song=self.fSongs[i]
            # look for last top-level entry
            if song[LIB_INDENT]==0:
                song[LIB_NEXTROW]=numFSongs
                break
            if processed[song[LIB_INDENT]]==False:
                song[LIB_NEXTROW]=numFSongs
                processed[song[LIB_INDENT]]=True


        # remove the dummy
        self.fSongs.pop(0)

        self.numVisEntries=len(filter(lambda entry: entry[LIB_INDENT]==0, self.fSongs))

    def updateSongs(self, songs):
        """Update the displayed songs and clears selection."""
        self.songs=songs
        if songs:
            self.numSongs = len(songs)
        else:
            self.numSongs = 0

        self.setMode(self.mode, self.groupByStr)

        self.redrawID=None
        QtCore.QCoreApplication.postEvent(self, DoUpdate())

    def selectedSongs(self):
        """Returns the list of selected songs."""
        ret=[]
        if self.mode=='playlist':
            cmp=lambda song: song._data['id']>=range[0] and song._data['id']<=range[1]
        elif self.mode=='library':
            cmp=lambda song: song._data['id']>=range[0] and song._data['id']<=range[1]
        for range in self.selIDs:
            # look for the songs in the current range
            songs=filter(cmp, self.songs)
            # add songs in range
            ret.extend(songs)
        return ret

    def killFilters(self):
        songs=self.songs
        self.songs=None
        while (len(self._filters)):
            # wait 'till everything's cleared
            doEvents()
        self.songs=songs


    # contains filters ready to be applied; only the top one will be used
    _filters=[]
    def filter(self, strFilter):
        """Filter songs according to $strFilter."""

        self._filters.append(strFilter)
        strFilter=self._filters[-1]

        try:
            if self.mode=='playlist':
                self.fSongs=filter(lambda song: song.match(strFilter), self.songs)
                self.numVisEntries=len(self.fSongs)
            else:
                self.groupBy(self.groupByStr, strFilter)
        except:
            # we might get here because self.songs is None
            pass
        self._filters=[]
        QtCore.QCoreApplication.postEvent(self, DoResize())

    def colorID(self, id, clr):
        """Color the row which contains song with id $id $clr."""
        self.clrID=[id, clr]
        self.redrawID=id

        QtCore.QCoreApplication.postEvent(self, DoUpdate())

    def selectRow(self, row):
        """Make $row the current selection."""
        self.selRows=[[row,row]]

        QtCore.QCoreApplication.postEvent(self, DoUpdate())

    def showColumn(self, column, show=True):
        """Hide or show column $column."""
        self.headers[column][2]=show

        self.update()

    def autoSizeColumn(self, column):
        """Resizes column $column to fit the widest entry in the non-filtered songs."""
        # we can't calculate it here, as retrieving the text-width can only
        # be done in the paintEvent method ...
        self.resizeColumn=column

        QtCore.QCoreApplication.postEvent(self, DoUpdate())

    def visibleSongs(self):
        """Get the songs currently visible."""
        ret=[]
        if self.mode=='playlist':
            for row in xrange(self.topRow, min(self.numSongs, self.topRow+self.numRows)-1):
                ret.append(self.fSongs[row])
        elif self.mode=='library':
            # note that if everything is folded, there'll be no songs!
            entries=self.fSongs
            index=self.libFirstVisRowIndex()
            count=0
            while index>=0 and index<len(entries) and count<self.numRows:
                entry=self.fSongs[index]
                if isinstance(entry[LIB_VALUE], Song):
                    ret.append(entry[LIB_VALUE])
                index=self.libIthVisRowIndex(index)
                count+=1
        return ret

    def ensureVisible(self, id):
        """Make sure the song with $id is visible."""
        if len(filter(lambda song: song.getID()==id, self.visibleSongs())):
            return
        row=0
        ok=False
        if self.mode=='playlist':
            # playlist mode is simple: just hop to the song with id!
            for song in self.fSongs:
                row+=1
                if song.getID()==id:
                    ok=True
                    break
        elif self.mode=='library':
            # library mode is more complex: we must find out how many rows are visible,
            # and expand the parents of the song, if necessary
            indLevel=0  # indicates what is the deepest level that is expanded for the current entry
                # thus if current indent<=indLevel, then it is visible
            for entry in self.fSongs:
                if entry[LIB_EXPANDED]==1:
                    indLevel=max(entry[LIB_INDENT]+1, indLevel)
                elif entry[LIB_EXPANDED]==0:
                    indLevel=min(entry[LIB_INDENT], indLevel)
                if entry[LIB_INDENT]<=indLevel:
                    row+=1

                #print "%s -> %s"%(str(indLevel), str(entry))
                if isinstance(entry[LIB_VALUE], Song) and entry[LIB_VALUE].getID()==id:
                    # expand parents
                    # must be expanded in reverse order, else we will count too many
                    # entries ...
                    parents=[]
                    while entry[LIB_PARENT]>=0:
                        entry=self.fSongs[entry[LIB_PARENT]]
                        parents.append(entry)
                        row-=1
                    parents.reverse()
                    for parent in parents:
                        self.libExpand(parent)
                    ok=True
                    break

        if ok:
            self.vScrollbar.setValue(row-self.numRows/2)
            self.update()


    def onVScroll(self, value):
        # 'if value<0' needed because minimum can be after init <0 at some point ...
        if value<0: value=0
        if value>self.numVisEntries:value=self.numVisEntries
        self.topRow=value

        self.update()

    def onHScroll(self, value):
        self.xOffset=-self.hScrollbar.value()*2
        self.update()

    def _pos2row(self, pos):
        return int(pos.y()/self.lineHeight)-1
    def _row2entry(self, row):
        entry=self.fSongs[0]
        try:
            while row>0:
                if entry[LIB_EXPANDED]:
                    entry=self.fSongs[entry[LIB_ROW]+1]
                else:
                    entry=self.fSongs[entry[LIB_NEXTROW]]
                row-=1
        except:
            return None
        return entry

    def focusOutEvent(self, event):
        self.update()
    def focusInEvent(self, event):
        self.update()
    def wheelEvent(self, event):
        if self.vScrollbar.isVisible():
            event.accept()
            numDegrees=event.delta() / 8
            numSteps=5*numDegrees/15
            self.vScrollbar.setValue(self.vScrollbar.value()-numSteps)

    def resizeEvent(self, event):
        # max nr of rows shown
        self.numRows=int(self.height()/self.lineHeight)

        # check vertical scrollbar
        if self.numRows>self.numVisEntries:
            self.vScrollbar.setVisible(False)
            self.vScrollbar.setValue(0)
        else:
            self.vScrollbar.setVisible(True)
            self.vScrollbar.setPageStep(self.numRows-2)
            self.vScrollbar.setMinimum(0)
            self.vScrollbar.setMaximum(self.numVisEntries-self.numRows+1)
            self.vScrollbar.resize(self.scrollbarWidth, self.height()-self.lineHeight-1)
            self.vScrollbar.move(self.width()-self.vScrollbar.width()-1, self.lineHeight-1)

        # check horizontal scrollbar
        self.scrollWidth=0
        if self.mode=='playlist':
            for hdr in self.headers:
                if hdr[2]:
                    self.scrollWidth+=hdr[1]

        if self.scrollWidth>self.width():
            self.hScrollbar.setVisible(True)
            self.hScrollbar.setMinimum(0)
            self.hScrollbar.setMaximum((self.scrollWidth-self.width())/2)
            self.hScrollbar.resize(self.width()-4, self.scrollbarWidth)
            self.hScrollbar.move(2, self.height()-self.hScrollbar.height()-1)

            # some changes because the hScrollbar takes some vertical space ...
            self.vScrollbar.resize(self.vScrollbar.width(), self.vScrollbar.height()-self.lineHeight)
            self.vScrollbar.setMaximum(self.vScrollbar.maximum()+1)

            self.numRows-=1
        else:
            self.hScrollbar.setVisible(False)
            self.hScrollbar.setValue(0)

    def libExpand(self, entry):
        if entry and entry[LIB_EXPANDED]==0:
            self.libToggle(entry)
    def libCollapse(self, entry):
        if entry and entry[LIB_EXPANDED]==1:
            self.libToggle(entry)

    def libToggle(self, entry):
        """Toggles expanded state. Returns new state"""
        expanded=entry[LIB_EXPANDED]
        if expanded!=2:
            # there was a '+' or a '-'!
            entry[LIB_EXPANDED]=(expanded+1)%2
            ret=entry[LIB_EXPANDED]
            # we must find out how many entries have appeared/disappeard
            # while collapsing.
            visibles=0  # how many new elements have appeared?
            i=entry[LIB_ROW]+1  # current element looking at
            indLevel=self.fSongs[i][LIB_INDENT]
            while i<=entry[LIB_NEXTROW]-1 and i<len(self.fSongs):
                entry2=self.fSongs[i]
                if entry2[LIB_EXPANDED]==1:
                    indLevel=max(entry2[LIB_INDENT]+1, indLevel)
                elif entry2[LIB_EXPANDED]==0:
                    indLevel=min(entry2[LIB_INDENT], indLevel)
                if entry2[LIB_INDENT]<=indLevel:
                    visibles+=1

                if entry2[LIB_EXPANDED]==0:
                    i=entry2[LIB_NEXTROW]
                else:
                    i+=1

            mult=1
            if ret==0:  mult=-1
            self.numVisEntries+=mult*visibles
            self.resizeEvent(None)
            return ret

        return 2

    def mousePressEvent(self, event):
        self.setFocus()
        pos=event.pos()
        row=self._pos2row(pos)

        done=False  # indicates whether some action has been done or not
        if self.mode=='playlist':
            self.scrollMult=1
            if row==-1:
                # we're clicking in the header!
                self.resizeCol=None
                x=0+self.xOffset
                i=0
                # check if we're clicking between two columns, if so: resize mode!
                for hdr in self.headers:
                    if hdr[2]:
                        x+=hdr[1]
                    if abs(x-pos.x())<4:
                        self.resizeCol=i
                        done=True
                    i+=1
        elif self.mode=='library':
            entry=self._row2entry(row+self.topRow)
            if not entry:
                entry=self.fSongs[len(self.fSongs)-1]
            if entry and pos.x()>(1+entry[LIB_INDENT])*self.indentation \
                    and pos.x()<(1+entry[LIB_INDENT]+3/2)*self.indentation:
                # we clicked in the margin, to expand or collapse
                self.libToggle(entry)
                done=True

        if done==False:
            self.selMode=True
            self.selIDs=[]
            self.selMiscs=[]
            if row==-1 and self.resizeCol==None:
                # we're not resizing, thus we can select all!
                self.selRows=[[0, len(self.fSongs)]]
            elif row>=0:
                # we start selection mode
                if self.mode=='playlist':
                    self.selRows=[[self.topRow+row,self.topRow+row]]
                elif self.mode=='library':
                    self.selRows=[[entry[LIB_ROW], entry[LIB_NEXTROW]-1]]
                self.selMode=True

        self.update()

    def mouseMoveEvent(self, event):
        pos=event.pos()
        row=self._pos2row(pos)
        if self.selMode:
            # we're in selection mode
            if row<0:
                # scroll automatically when going out of the widget
                row=0
                if self.topRow>0:
                    self.scrollMult+=0.1
                    jump=int(self.scrollMult)*int(abs(pos.y())/self.lineHeight)
                    self.vScrollbar.setValue(self.vScrollbar.value()-jump)
                    row=jump
            elif row>=self.numRows:
                # scroll automatically when going out of the widget
                self.scrollMult+=0.1
                jump=int(self.scrollMult)*int(abs(self.height()-pos.y())/self.lineHeight)
                self.vScrollbar.setValue(self.vScrollbar.value()+jump)
                row=self.numRows-jump
            else:
                # reset the scrollMultiplier
                self.scrollMult=1

            if self.mode=='playlist':
                self.selRows[0][1]=row+self.topRow
            elif self.mode=='library':
                self.selRows[0][1]=self.libIthVisRowIndex(self.libIthVisRowIndex(0,self.topRow), row)
            self.update()
        elif self.resizeCol!=None:
            row-=1
            # ohla, we're resizing a column!
            prev=0
            # calculate where we are
            for i in xrange(self.resizeCol):
                hdr=self.headers[i]
                if hdr[2]:
                    prev+=hdr[1]
            self.headers[self.resizeCol][1]=pos.x()-prev-self.xOffset
            # minimum width check?
            if self.headers[self.resizeCol][1]<self.minColumnWidth:
                self.headers[self.resizeCol][1]=self.minColumnWidth
            self.resizeEvent(None)
            self.update()

    def mouseReleaseEvent(self, event):
        if self.selMode and len(self.selRows):
            # we were selecting, but now we're done.
            # We have to transform one range of rows
            # into range of selected IDs
            # The problem is that the list can be filtered, and that
            # consequtive, visible rows aren't always directly
            # consequtive in the unfiltered list.
            self.selMode=False  # exit selection mode
            fSongs=self.fSongs
            self.selMiscs=[]
            ranges=[]
            curRange=[]
            # loop over all rows that are selected
            for entry in fSongs[min(self.selRows[0]):max(self.selRows[0])+1]:
                song=None
                if isinstance(entry,Song):
                    song=entry
                elif isinstance(entry[LIB_VALUE],Song):
                    song=entry[LIB_VALUE]
                else:
                    self.selMiscs.append(entry[LIB_ROW])

                if song!=None:
                    id=song.getID()
                    # is this song directly after the previous row?
                    if len(curRange)==0 or curRange[-1]+1==id:
                        curRange.append(id)
                    else:
                        ranges.append(curRange)
                        curRange=[id]
            if len(curRange):
                ranges.append(curRange)
            # clean up ranges
            self.selRows=[]
            self.selIDs=[]
            for range in ranges:
                self.selIDs.append([range[0], range[-1]])
            self.update()

        elif self.resizeCol!=None:
            # store this new width!
            self.saveColumnWidth(self.resizeCol)
            # we're not resizing anymore!
            self.resizeCol=None
            self.update()
    def saveColumnWidth(self, col):
        settings.set('l%s.%s.width'%(self.name,self.headers[col][0]), self.headers[col][1])
    def mouseDoubleClickEvent(self, event):
        pos=event.pos()
        row=self._pos2row(pos)
        if row>=0:
            self.onDoubleClick()
        else:
            # auto-size column
            x=0+self.xOffset
            i=0
            for hdr in self.headers:
                if hdr[2]:
                    x+=hdr[1]
                if abs(x-pos.x())<4:
                    self.autoSizeColumn(i)
                    break
                i+=1

    def _paintPlaylist(self, p):
        self.redrawID=None


        lineHeight=self.lineHeight
        margin=self.margin
        vmargin=self.vmargin
        selRows=self.selRows
        width=self.width()
        if self.vScrollbar.isVisible():
            width-=self.scrollbarWidth

        if self.resizeColumn!=None:
            # we're autoresizing!
            # must be done here, because only here we can check the textwidth!
            # This is because of limitations it can be only be done in paintEvent
            hdr=self.headers[self.resizeColumn][0]
            w=self.minColumnWidth
            # loop over all visible songs ...
            for song in self.fSongs:
                rect=p.boundingRect(10,10,1,1, QtCore.Qt.AlignLeft, song.getTag(hdr))
                w=max(rect.width(), w)
            self.headers[self.resizeColumn][1]=w+2*margin
            self.saveColumnWidth(self.resizeColumn)
            self.resizeColumn=None
            self.resizeEvent(None)
        if self.redrawID!=None:
            # only update one row
            y=lineHeight
            for row in xrange(self.topRow, min(self.numVisEntries, self.topRow+self.numRows)):
                if self.fSongs[row]._data['id']==self.redrawID:
                    self._paintPlaylistRow(p, row, y, width)
                y+=lineHeight

            self.redrawID=None
            return

        # paint the headers!
        p.fillRect(QtCore.QRect(0,0,width+self.vScrollbar.width(),lineHeight), self.palette().brush(QPalette.Button))
        p.drawRect(QtCore.QRect(0,0,width+self.vScrollbar.width()-1,lineHeight-1))
        x=margin+self.xOffset
        p.setPen(self.palette().color(QPalette.ButtonText))
        for hdr in self.headers:
            if hdr[2]:
                p.drawText(x, vmargin, hdr[1], lineHeight, QtCore.Qt.AlignLeft, hdr[0])
                x+=hdr[1]
                p.drawLine(QtCore.QPoint(x-margin,0), QtCore.QPoint(x-margin,lineHeight))

        if self.songs==None:
            return
        # fill the records!
        y=lineHeight
        for row in xrange(self.topRow, min(self.numVisEntries, self.topRow+self.numRows)):
            self._paintPlaylistRow(p, row, y, width)
            y+=lineHeight
        if y<self.height():
            # if we're short on songs, draw up the remaining area in background color
            p.fillRect(QtCore.QRect(0,y,width,self.height()-y), QtGui.QBrush(self.clrBg))

    def _paintPlaylistRow(self, p, row, y, width):
        """Paint row $row on $p on height $y and with width $width."""
        song=self.fSongs[row]
        lineHeight=self.lineHeight
        margin=self.margin
        vmargin=self.vmargin
        id=song._data['id']

        # determine color of row. Default is row-color, but can be overridden by
        # (in this order): selection, special row color!
        clr=self.colors[row%2]  # background color of the row
        clrTxt = self.palette().color(QPalette.Text) # color of the printed text
        # is it selected?
        values=[]
        if self.selMode:
            checkID=row
            values=self.selRows
        else:
            checkID=id
            values=self.selIDs
        # if values==[], it won't run!
        for range in values:
            # is selected if in range, which depends on the selection-mode
            if checkID>=min(range) and checkID<=max(range):
                clr=self.clrSel
                clrTxt = self.palette().color(QPalette.HighlightedText) # color of the printed text
        # it has a VIP-status!
        if id==int(self.clrID[0]):
            clrTxt = self.palette().color(QPalette.HighlightedText) # color of the printed text
            clr=self.clrID[1]

        # draw the row background
        p.fillRect(QtCore.QRect(2, y, width-3, lineHeight), QtGui.QBrush(clr))

        # draw a subtile rectangle
        p.setPen(self.palette().color(QPalette.Highlight))
        p.drawRect(QtCore.QRect(2, y, width-3, lineHeight))

        # draw the column
        x=margin+self.xOffset
        for hdr in self.headers:
            if hdr[2]:
                # only if visible, duh!
                # rectangle we're allowed to print in
                text=song.getTag(hdr[0])
                if type(text)!=str and type(text)!=unicode:
                    text=str(text)
                rect=p.boundingRect(x, y, hdr[1]-margin, lineHeight, QtCore.Qt.AlignLeft, text)
                p.setPen(clrTxt)
                p.drawText(x, y+vmargin, hdr[1]-margin, lineHeight, QtCore.Qt.AlignLeft, text)
                if rect.width()>hdr[1]-margin:
                    # print ellipsis, if necessary
                    p.fillRect(x+hdr[1]-15,y+1,15,lineHeight-1, QtGui.QBrush(clr))
                    p.drawText(x+hdr[1]-15,y+vmargin,15,lineHeight-1, QtCore.Qt.AlignLeft, "...")
                x+=hdr[1]
                p.setPen(self.palette().color(QPalette.Base))
                p.drawLine(QtCore.QPoint(x-margin,y), QtCore.QPoint(x-margin,y+lineHeight))

    def libFirstVisRowIndex(self):
        """Returns the index of the first visible row in library mode."""
        # if not in library mode, the unthinkable might happen! Wooo!
        # TODO find better initial value
        row=0   # the visible rows we're visiting
        index=0 # what index does the current row have
        entries=self.fSongs

        while index<len(entries):
            if row>=self.topRow:
                break
            entry=entries[index]
            if entry[LIB_EXPANDED]==0:
                index=entry[LIB_NEXTROW]
            else:
                index+=1
            row+=1
        return index
    def libIthVisRowIndex(self, index, i=1):
        """Returns the index of the $i-th next row after $index that is visible (or -1) in library mode."""
        entries=self.fSongs
        while i>0 and index<len(entries):
            i-=1
            entry=self.fSongs[index]
            if entry[LIB_EXPANDED]==0:
                if index<0:
                    return -1
                index=entry[LIB_NEXTROW]
            else:
                index+=1

        return index

    def _paintLibrary(self, p):
        width=self.width()
        height=self.height()
        lineHeight=self.lineHeight
        margin=self.margin
        vmargin=self.vmargin

        # paint the headers!
        p.fillRect(QtCore.QRect(0,0,width+self.vScrollbar.width(),lineHeight), self.palette().brush(QPalette.Button))
        p.drawRect(QtCore.QRect(0,0,width+self.vScrollbar.width()-1,lineHeight-1))
        p.setPen(self.palette().color(QPalette.ButtonText))
        p.drawText(margin, vmargin, width, lineHeight, QtCore.Qt.AlignLeft, self.groupByStr.replace('$', ''))

        entries=self.fSongs

        y=lineHeight
        x=margin
        indent=self.indentation
        index=self.libFirstVisRowIndex()
        row=0
        while index<len(entries) and y<height:
            entry=entries[index]

            level=entry[LIB_INDENT]
            isSong=isinstance(entry[LIB_VALUE], Song)

            if isSong:
                prefix=''
                text="%s %s"%(entry[LIB_VALUE].getTrack(), entry[LIB_VALUE].getTitle())
            else:
                if entry[LIB_EXPANDED]==1:  prefix='-'
                elif entry[LIB_EXPANDED]==0:    prefix='+'
                text=entry[LIB_VALUE]

            clr=self.colors[row%2]  # background color of the row
            clrTxt = self.palette().color(QPalette.Text)

            values=[]
            if self.selMode:
                checkID=index
                values=self.selRows
            elif self.selMode==False and isSong:
                checkID=entry[LIB_VALUE].getID()
                values=self.selIDs

            # if values==[], then it won't run!
            for range in values:
                # is selected if in range, which depends on the selection-mode
                if checkID>=min(range) and checkID<=max(range):
                    clr=self.clrSel
                    clrTxt = self.palette().color(QPalette.HighlightedText)

            for i in self.selMiscs:
                if index==i:
                    clr=self.clrSel
                    clrTxt = self.palette().color(QPalette.HighlightedText)

            # it has a VIP-status!
            if isSong and entry[LIB_VALUE].getID()==int(self.clrID[0]):
                clrTxt = self.palette().color(QPalette.HighlightedText)
                clr=self.clrID[1]

            left=x+indent*(1+level)
            top=y+vmargin
            p.fillRect(QtCore.QRect(left,y,width-3,lineHeight), clr)
            p.setPen(clrTxt)
            p.drawText(left, top, 15, lineHeight, QtCore.Qt.AlignLeft, prefix)
            p.drawText(left+15, top, width, lineHeight, QtCore.Qt.AlignLeft, text)

            obj=None
            if level<len(self.levels):
                if self.levels[level][0:len('$artist')]=='$artist':
                    obj=self.wgGfxArtist
                elif self.levels[level][0:len('$album')]=='$album':
                    obj=self.wgGfxAlbum

            if obj:
                obj.render(p, QtCore.QRectF(indent*level+1,y+1,lineHeight-1,lineHeight-1))

            y+=lineHeight
            row+=1
            index=self.libIthVisRowIndex(index)
            if index<0:
                break

    _mutex=QtCore.QMutex()
    _paintCnt=0
    def paintEvent(self, event):
        self._mutex.lock()
        if self._paintCnt:
            self._mutex.unlock()
            return
        self._paintCnt=1
        self._mutex.unlock()


        p=QtGui.QPainter(self)

        # for the moment, redraw everything ...
        p.fillRect(QtCore.QRect(0,0,self.width(),self.height()), QtGui.QBrush(self.clrBg))
        if self.mode=='playlist':
            self._paintPlaylist(p)
        elif self.mode=='library':
            self._paintLibrary(p)

        # draw a nice line around the widget!
        p.drawRect(QtCore.QRect(0,0,self.width()-1,self.height()-1))
        if self.hasFocus():
            p.drawRect(QtCore.QRect(1,1,self.width()-3,self.height()-3))
        else:
            p.setPen(self.palette().color(QPalette.Button))
            p.drawRect(QtCore.QRect(1,1,self.width()-3,self.height()-3))

        self._paintCnt=0

        text=None
        if len(self._filters):
            text="Please wait while filtering ... (%i)"%(len(self._filters))
        #text='%s - %s' % (self.selMiscs, '')
        #text='%s - %s - %s' % (str(self.topRow), str(self.selRows), str(self.selIDs))
        #text='%s - %s'%(str(self.topRow), str(self.numVisEntries))
        if text:
            r=QtCore.QRect(10,self.height()-40,self.width()-20,20)
            p.fillRect(r, self.palette().brush(QPalette.Base))
            p.drawText(r,QtCore.Qt.AlignLeft, text)

