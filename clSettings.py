from traceback import print_exc
from PyQt4 import QtCore

class Settings:
    fileName=None
    lines=[]

    def __init__(self, file='settings.txt'):
        self.fileName=file
        self.read()

    def __del__(self):
        self.write()

    def get(self, name, default=None):
        for line in self.lines:
            if line[0:len(name)]==name:
                return line[len(name):].strip()
        return default

    def set(self, name, value):
        newvalue="%s\t%s"%(name,value)
        for i in xrange(len(self.lines)):
            line=self.lines[i]
            if line[0:len(name)]==name:
                self.lines[i]=newvalue
                return
        # new value to write
        self.lines.append(newvalue)

    def getIntTuple(self, name):
        """Note this might return an exception!"""
        val=self.get(name)
        i=val.find(' ')
        x,y=int(val[0:i]), int(val[i:])
        return x,y

    def setIntTuple(self, name, val1, val2):
        self.set(name, "%i %i"%(val1, val2))

    def read(self):
        try:
            self.lines=[]
            f=open(self.fileName)
            while True:
                line=f.readline()
                if line=='':    break
                self.lines.append(line.strip().replace('$NEWLINE', '\n'))
            f.close()
        except IOError:
            pass
        except:
            print_exc()
    def write(self):
        f=open(self.fileName, 'wb')
        for line in self.lines:
            f.write("%s\n"%(line.replace('\n', '$NEWLINE')))
        f.close()


settings=Settings()
