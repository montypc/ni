from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import QVariant
import time
from misc import *
from traceback import print_exc

class winConnect(QtGui.QWidget):
    txtHost=None
    txtPort=None
    lblInfo=None
    _timerID=None
    monty = None
    settings = None


    def __init__(self,parent):
        QtGui.QWidget.__init__(self, parent)
        self.settings = QtCore.QSettings(ORGNAME, APPNAME)
        self.txtHost  = QtGui.QLineEdit(self.settings.value('MPD/host', QVariant('localhost')).toString())
        self.txtPort  = QtGui.QLineEdit(self.settings.value('MPD/port', QVariant('6600')).toString())
        self.txtPort.setValidator(QtGui.QIntValidator(1, 65535, self.txtPort))
        self.lblInfo  = QtGui.QLabel("connecting ...")
        self.monty = parent.monty

        frame=QtGui.QVBoxLayout()
        inputs=QtGui.QHBoxLayout()

        frame.addLayout(inputs)
        frame.addWidget(self.lblInfo)

        inputs.addWidget(self.txtHost)
        inputs.addWidget(self.txtPort)

        self.setWindowIcon(QtGui.QIcon(appIcon))
        self.setWindowTitle('Connect to mpd')
        self.setLayout(frame)
        self.resize(200,80)
        self.center()
        doEvents()

        self.monty.addListener('onReady', self.onReady)
        self.monty.addListener('onConnect', self.onConnect)

    def center(self):
        screen = QtGui.QDesktopWidget().screenGeometry()
        size = self.geometry()
        self.move((screen.width()-size.width())/2, (screen.height()-size.height())/2+100)

    def monitor(self):
        self.txtHost.setEnabled(True)
        self.txtPort.setEnabled(True)
        if self._timerID==None:
            self._timerID=self.startTimer(200)
        if self.isVisible()==False:
            self.show()
        self.activateWindow()
        self.raise_()
        doEvents()


    def onConnect(self, params):
        if self._timerID:
            self.killTimer(self._timerID)
            self._timerID=None
        self.lblInfo.setText('Connected!\nRestoring library and playlist ...')
        doEvents()
        self.settings.setValue('MPD/host', QVariant(self.txtHost.text()))
        self.settings.setValue('MPD/port', QVariant(self.txtPort.text()))
        self.txtHost.setEnabled(False)
        self.txtPort.setEnabled(False)
        doEvents()

    def onReady(self, params):
        self.hide()

    def timerEvent(self, event):
        host = str(self.txtHost.text())
        port = int(self.txtPort.text())

        self.lblInfo.setText('Trying to connect to '+host+':'+str(port)+' ...')
        doEvents()
        self.monty.connect(host, port)
        doEvents()

    def windowActivationChange(self, bool):
        self.activateWindow()
        self.raise_()
        doEvents()
